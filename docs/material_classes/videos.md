# Vídeos

## Usuarios

### Cómo crear usuarios y grupos

Vídeo explicativo de cómo crear usuarios y grupos. Para más información sobre la gestión de usuarios (cómo crear usuarios y grupos), pulsa [aquí](https://isard.gitlab.io/isardvdi-docs/manager/manage_user.es/)

<iframe width="560" height="315" src="https://www.youtube.com/embed/gYo2MXWjJNw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Cómo añadir cuotas/límites

Vídeo explicativo de cómo añadir cuotas/límites a usuarios, grupos y categorías. Para más información sobre las cuotas y límites, pulsa [aquí](https://isard.gitlab.io/isardvdi-docs/manager/quotas_limits.es/)

<iframe width="560" height="315" src="https://www.youtube.com/embed/dbQPuogz6qY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

![](./material_classes.images/cuotas.png)
#### Cuotas

La cuota define cuantos recursos puede tener como máximo cada usuario (a no ser que se le haya aplicado un cuota concreta al usuario). 

**Por ejemplo:**

Si tengo un grupo con una cuota de grupo de 10 escritorios y el usuario no tiene ninguna cuota aplicada, el total de escritorios **que puede crear cada usuario perteneciente al grupo** no debe pasar de 10. Esto significa que en cuanto un usuario va a crear un escritorio se comprueba que al crear el escritorio no sobrepasará esos 10 escritorios **suyos**.

#### Límites

El límite define cuantos recursos puede tener como máximo un grupo/categoría.

**Por ejemplo:**

Si tengo un grupo con límite de 10 escritorios y el grupo no tiene ninguna cuota aplicada, el total de escritorios **del grupo** no debe sobrepasar 10. Esto significa que en cuanto un usuario va a crear un escritorio se comprueba que al crear el escritorio el total del grupo no sobrepasará esos 10 escritorios, por lo que no hay un control sobre cuántos tiene concretamente cada usuario. De esta manera se ofrece una mayor flexibilidad, ya que podrían darse muchas combinaciones:

- Un usuario podría tener 10 y consecuentemente ningún otro usuario podría crear ninguno.
- Un usuario podría tener 2 escritorios, otro 7 y otro 1
- Un usuario podría tener 9 escritorios y otro 1.
- Un usuario podría tener 6 escritorios, otro 3 y otro 1.




## Uso
### Cómo crear un escritorio y una plantilla

Vídeo explicativo de cómo crear un escritorio en IsardVDI y crear plantilla para que otros usuarios o tú mismo la puedan utilizar. Para más información sobre la creación de escritorios pulsa [aquí](https://isard.gitlab.io/isardvdi-docs/user/create_desktop.es/) y [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/templates.es/) para más información sobre la creación de plantillas.

<iframe width="560" height="315" src="https://www.youtube.com/embed/fLppnZiGQ6c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Cómo duplicar una plantilla y compartirla

Vídeo explicativo de cómo duplicar una plantilla y compartirla con usuarios. Para más información sobre duplicar plantillas, pulsa [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/templates.es/#duplicar-plantilla).

<iframe width="560" height="315" src="https://www.youtube.com/embed/vngDjLMYSa0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>


### Cómo crear un despliegue

Vídeo explicativo de cómo crear un despliegue con una plantilla para otros usuarios. Para más información sobre los despliegues, pulsa [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/deployments.es/).

<iframe width="560" height="315" src="https://www.youtube.com/embed/xD4Dqyj2ENk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>