# Reservas GPU

En este apartado encontraremos cómo usar vGPUs (GPUs virtuales) asociadas a recursos GPUs de tarjetas Nvidia, cómo añadir este tipo de hardware a los escritorios virtuales y la gestión de las reservas asociada.

## Explicación de la tecnología vGPUs

IsardVDI permite trabajar con la tecnología de **[Nvidia vWS](https://www.nvidia.com/es-es/design-visualization/virtual-workstation/)** que permite disponer de recursos de una tarjeta gráfica asociados a un escritorio virtual. Esta tecnología nos permite poder ejecutar software que requiere **recursos de gpu dedicados**, como programas de diseño 3D, animación, edición de vídeo, CAD o diseño industrial.

Cada tarjeta gráfica puede dividirse en diferentes perfiles con una cantidad de memoria reservada. En función del perfil seleccionado se dispone de más o menos tarjetas virtuales. En el siguiente gráfico se observa un servidor, que tiene una serie de tarjetas GPUs instaladas, cada tarjeta puede dividirse en vGPUs (VirtualGPUs) que pueden mapearse a cada máquina virtual.

![stack_vgpu](./vgpus.images/o6SWmXS.png)

Veamos con un ejemplo el funcionamiento. Disponemos de una tarjeta **[nvidia A40](https://www.nvidia.com/es-es/data-center/a40/)** y queremos usar un programa como el SolidWorks para diseñar piezas 3D. Queremos un perfil con 2GB de memoria gráfica dedicada. Esta tarjeta gráfica dispone de 48GB de memoria, que puede dividirse en perfiles "2Q": El primer carácter hace referencia a la cantidad de memoria reservada (2GB) y el segundo carácter (Q) hace referencia al licenciamiento vWS que es el modo en que la tarjeta virtual se configura para trabajos creativos y técnicos que usan la tecnología **[Quadro de Nvidia](https://docs.nvidia.com/grid/10.0/grid-vgpu-user-guide/index.html#supported-gpus-grid-vgpu)**. Los diferentes perfiles y modos de funcionamiento disponibles para esta tarjeta están accesibles en el [manual de nvidia con los perfiles para la A40](https://docs.nvidia.com/grid/13.0/grid-vgpu-user-guide/index.html#vgpu-types-nvidia-a40). En el caso de 2Q tenemos:

- Memoria dedicada y reservada: 2GB
- Número máximo de vGPUs por GPU: 24

Diagramas de funcionamiento de la tecnología Nvidia vGPU:

![](./vgpus.images/9qceuMB.png)

![](./vgpus.images/qtAbMD3.png)

## ¿Por qué un sistema de reservas?

IsardVDI está pensado para que los usuarios de forma autónoma puedan crear y desplegar escritorios, el límite de cuantos escritorios pueden correr de forma simultánea en el sistema viene condicionado por los recursos disponibles. Si los escritorios no tienen vGPUs el límite suele venir por la cantidad de memoria RAM disponible en los hypervisores, siendo aconsejable que siempre la capacidad de memoria y cpus de estos servidores sea suficiente para sostener la concurrencia de escritorios. Podemos limitar la cantidad de recursos que otorgamos a cada usuario, grupo o categoría con las cuotas y límites que IsardVDI permite establecer.

En el caso de las GPUs son recursos caros (está el coste de la tarjeta y el coste del licenciamiento) y para acceder a estos recursos se ha establecido un sistema de reservas, con las siguientes características:
- El administrador puede **planificar horarios con diferentes perfiles** aplicados para cada tarjeta disponible, permitiendo que en un determinado horario haya un perfil con mucha memoria dedicada y pocos usuarios (por ejemplo para lanzar una renderización) y en otra franja con poca memoria y muchos usuarios (por ejemplo para realizar una formación)
- Hay un **sistema de permisos** granular que permite definir qué usuarios tienen permiso a usar un determinado "profile" de una tarjeta. Permite que sólo un grupo reducido de usuarios puedan acceder a los perfiles con más memoria. 
- Un usuario puede reservar un escritorio para una determinada franja horaria con un tiempo mínimo previo a la reserva y una **duración máxima de la reserva**. Permite que se puedan hacer reservas con poco tiempo de antelación previo y que no puedan reservarse franjas superiores a número reducido de horas.
- Un usuario avanzado puede hacer una **reserva para un despliegue**, reservando tantas GPUs como escritorios contiene el despliegue
- Hay un sistema de **prioridades de reservas**, que permite poner reglas para que unos usuarios puedan sobreescribir y revocar reservas de otros. Esta opción es útil si queremos fomentar que la gpu se utilice por muchos usuarios, pero queremos garantizar que para unos determinados grupos nunca les falte reserva. Por ejemplo, si queremos que un grupo de usuarios que usan un programa de diseño 3D puedan revocar reservas de otros usuarios.



## Sólo Visor RDP con escritorios con GPU

Los escritorios con GPU hacen uso del controlador de tarjeta GPU y, por lo tanto **sólo es posible conectarse a ellos mediante el visor RDP o el visor RDP web**.

El botón **Iniciar** del escritorio se **activará** cuando llegue la hora de la reserva y se **desactivará** al finalizar dicha reserva. Además, al finalizar la reserva, el escritorio se **apagará automáticamente**.

## Escritorios con GPU

**Información importante** sobre los escritorios con GPU:

- Si un escritorio tiene GPU, es recomendable utilizar sólo los **visores con RDP**
- A la hora de hacer una reserva **hace falta una antelación de 2 minutos**
- El botón de **Iniciar** se activará al llegar la hora de la reserva y se desactivará al final de la reserva. Además, en finalizar el horario de reserva se apagará automáticamente el escritorio.
### Usuarios User, Advanced, Manager
#### Crear un escritorio con GPU

En la vista **Escritorios** del usuario, se crea uno nuevo mediante el botón ![](vgpus.images/1.png)

Se selecciona la plantilla base y en el apartado de **Opciones avanzadas** nos desplazamos hasta llegar al apartado **Hardware**.

Es importante que la configuración **Videos** tenga la opción **Only GPU** marcada. En los escritorios windows con los drivers de NVIDIA instalados no es compatible tener a la vez una tarjeta de vídeo QXL y una tarjeta vGPU.

![](./vgpus.images/JmLGOwA.png)

![](./vgpus.images/xKnsVfR.png)


En el siguiente apartado **Reservables** más abajo, se asigna el **perfil** de GPU deseado, por ejemplo A40-1Q (1024MB de memoria)

![](./vgpus.images/owCrz49.png)

NOTA: *Para poder reservar e iniciar un escritorio con un perfil, el administrador debe haberlo planificado previamente*

#### Editar un escritorio para asignarle GPU

Solamente hay que establecer las configuraciones del escritorio **Hardware - Videos** con valor **'Only GPU'** y asignar en **Reservables** el **perfil** de GPU adecuado.

![](./vgpus.images/ooITVZl.png)

![](./vgpus.images/xKnsVfR.png)

![](./vgpus.images/owCrz49.png)

![](./vgpus.images/og2br7f.png)

Si queremos dar más recursos de gpu, podemos editar el escritorio y cambiar solamente la parte de "Reservables" seleccoinando un perfil con más memoria.

![](./vgpus.images/P6fEDvO.png)

#### Editar y configurar redes y visores para escritorios con GPU

Para poder utilizar un escritorio con GPU, el escritorio debe de tener seleccionado en la configuración solamente los visores de tipo RDP.

Si hay un problema con el sistema operativo y nunca se llega a obtener dirección IP, no se podrá acceder por RDP. 

#### Reservar un escritorio

Es necesario realizar las **reservas** de los **escritorios** con vGPU. El administrador habrá planificado unos perfiles de GPU en las diferentes tarjetas disponibles y el usuario podrá hacer reservas siempre que tenga permisos y coincida el perfil de su vGPU con una planificación disponible.

Se accede a reservar un escritorio con GPU desde el botón ![](vgpus.images/y7hK2er.png) 

![](./vgpus.images/7g0i8sW.png)

También se pueden hacer desplegando el menú en la parte superior derecha de la tarjeta del escritorio, pulsando el icono ![](vgpus.images/94peRhM.png). Este icono permitirá también acceder a crear más reservas a lo largo de la semana/meses:

![](./vgpus.images/ldHi2CB.png)

Nos da acceso a la vista semanal de la **disponibilidad** (podemos cambiar la vista a mensual o diaria), donde se encuentran dos columnas por cada día de la semana. En la columna de la izquierda aparece la disponibilidad para el perfil de tarjeta, y en la columna de la derecha es donde aparecerán las reservas que tenemos para ese escritorio.

En la figura se aprecia que existe **disponibilidad** durante la semana y no hay hecha **ninguna** reserva.

![](./vgpus.images/i90aqKf.png)

Las reservas pueden crearse mediante el botón ![](vgpus.images/2.png) o mediante el cursor, **clicando en la franja "Reservas" y arrastrando** para seleccionar el rango de hora deseado. En el formulario que aparecerá se puede ajustar el rango de fechas y horas de duración de la reserva que se quiera realizar.

![](./vgpus.images/kTl9XSX.png)

Una vez hecha la reserva, aparecerá en la columna de la derecha de cada jornada. 

![](./vgpus.images/QRwZ1mr.png)

#### Editar o eliminar reserva

Para eliminar una reserva se puede pulsar en la franja de reserva y aparece una opción de editar junto con un botón de eliminar. No se puede modificar una reserva que ya está en curso.

![](./vgpus.images/6YqdhPL.png)








