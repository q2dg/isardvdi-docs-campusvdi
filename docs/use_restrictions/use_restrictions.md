# Restricciones de uso

Para garantizar un uso más controlado y equitativo de nuestros recursos se han aplicado unas cuotas que afectan a todas las categorías. Esto significa que cada categoría tendrá una asignación de recursos establecida, con el objetivo de evitar la saturación y asegurar que todos los usuarios tengan acceso a la plataforma de manera adecuada. 

Entendemos que esta medida puede generar algunas preguntas o inquietudes, por lo que estamos aquí para ayudar y brindar asistencia. Si tienes dudas sobre cómo funcionarán las cuotas o si tienes alguna preocupación específica, no dudes en contactarnos.



## Cuotas aplicadas

De momento aplicaremos estas cuotas generales a todas las categorías:

|Cuota de creación| Cantidad de recursos que el usuario podrá crear|
| -----------|:-----------: |
|Desktops (Cantidad de escritorios que el usuario puede crear.)| 10|
|Templates (Cantidad de plantillas que el usuario puede crear.)| 20|
|Media (Cantidad de medios que el usuario puede subir o descargar.)| 5|

!!! Info "Info"
    Esto es individual: Cada usuario podrá crear 10 escritorios, 20 plantillas y 5 media subidos (isos)

|Cuota de escritorios en ejecución individual| Cantidad de recursos que el usuario podrá ejecutar a la vez|
| -----------|:-----------: |
|Concurrent (Cantidad total de escritorios que se pueden iniciar a la vez.)| 4|
|vCPUs (Cantidad total de vCPUS de los escritorios iniciados que se pueden utilizar a la vez.)| 8|
|RAM (Cantidad total de memoria vRAM de los escritorios iniciados que se pueden utilizar a la vez.)| 16|

!!! Info "Info"
    Esto es individual: Cada usuario podrá tener 4 escritorios arrancados, 8 de CPU y 16 GB de RAM a la vez.

|Cuota de tamaño| Tamaño de recursos que el usuario puede tener|
| -----------|:-----------: |
|Disk Size (Medida máxima permitida al crear un escritorio desde un medio.)| 100|
|Total (Medida total que se puede utilizar considerando escritorios, plantillas y medios.)| 999|
|Soft Size (Una vez ocupada esta cantidad, se avisará al usuario.)| 900|

!!! Info "Info"
    Esto es individual: Cada usuario podrá tener estos tamaños de recursos.

## Información

!!! Warning "Aclaraciones importantes"

    Los escritorios creados a través de despliegues no se consideran en la cuota de usuario.

    Los recursos compartidos con el usuario no se consideran en la cuota de usuario.

    Cuando se inicia un escritorio desde un despliegue, se considera la cuota del usuario. Por lo tanto, si el usuario no tiene suficiente cuota para iniciar el escritorio, se mostrará un error, incluso si el propietario del despliegue tiene suficiente cuota. La lógica detrás de este comportamiento es la propiedad del escritorio, el propietario del escritorio es el usuario, a pesar de que fue creado por el propietario del despliegue.


!!! Important "Para más información"
    Para más información sobre las cuotas o límites, pulsar [aquí](https://isard.gitlab.io/isardvdi-docs/manager/quotas_limits.es/)